package com.androidegitim.ekranyonu;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        PORTRE GORUNUM ICIN
//        -manifest
//              android:screenOrientation="portrait"
//        -activity
//              setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

//        LANDSCAPE GORUNUM ICIN
//        -manifest
//              android:screenOrientation="landscape"
//        -activity
//              setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);


        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_main);
            Toast.makeText(getApplicationContext(), "PORTRE", Toast.LENGTH_SHORT).show();

        } else {
            setContentView(R.layout.activity_main_land);
            Toast.makeText(getApplicationContext(), "LANDSCAPE", Toast.LENGTH_SHORT).show();
        }
    }
}
